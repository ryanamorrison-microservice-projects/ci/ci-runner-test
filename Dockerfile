FROM ubuntu:22.04
MAINTAINER ryan@ryanamorrison.com
LABEL "author"="ryanamorrison"
LABEL "Date"="2023-12-17"
RUN apt -y update && apt -y upgrade
RUN apt -y install curl
RUN curl https://dl.google.com/go/go1.21.4.linux-amd64.tar.gz -o /tmp/go1.21.4.linux-amd64.tar.gz 
RUN echo "73cac0215254d0c7d1241fa40837851f3b9a8a742d0b54714cbdfb3feaf8f0af /tmp/go1.21.4.linux-amd64.tar.gz" | sha256sum --check 
RUN rm -rf /usr/local/go && tar -C /usr/local -xzf /tmp/go1.21.4.linux-amd64.tar.gz 
RUN echo 'export PATH=$PATH:/usr/local/go/bin' >> ~/.bashrc
RUN /usr/local/go/bin/go version
CMD ["/bin/bash"]

