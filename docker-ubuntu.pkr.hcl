packer {
  required_plugins {
    docker = {
      version = ">= 1.0.8"
      source = "github.com/hashicorp/docker"
    }
  }
}

variable "docker_image" {
  type = string
  default = "ubuntu:jammy"
}

variable "ci_repo" {
  type = string
  default = env("CI_REG_NAME")
  validation {
    condition = length(var.ci_repo) > 0
    error_message = "Provide the CI repo name via the CI_REG_NAME variable."
  }
}

variable "ci_image" {
  type = string
  default = env("CI_IMAGE_TAG")
  validation {
    condition = length(var.ci_image) > 0
    error_message = "Provide the CI image name via the CI_IMAGE_TAG variable."
  }
}

variable "image_version" {
   type = string
   default = "v0.0.1"
   validation {
     condition = length(var.image_version) > 0
     error_message = "Provide the image_version var via --var image_version= on the CLI or via a vars file."
   }
}

source "docker" "ubuntu" {
  image  = var.docker_image 
  commit = true
}

build {
  name    = "test-packer"
  sources = [
    "source.docker.ubuntu",
  ]
  provisioner "shell" {
    environment_vars = [
      "VAR_BUILD_DIR=app",
    ]
    inline = [
      "echo creating $VAR_BUILD_DIR directory",
      "mkdir /$VAR_BUILD_DIR",
    ]
  }
  provisioner "shell" {
    inline = [
      "echo running ${var.docker_image } Docker image.",
    ]
  }
  post-processor "docker-tag" {
    repository = var.ci_repo
    tags = [var.ci_image,var.image_version]
    only = ["docker.ubuntu"]
  }
}


